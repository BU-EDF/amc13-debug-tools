#!/usr/bin/perl
#
# read a "Wu" dump and a Wu "release notes" file and produce a simple report
#
# usage:  dump-analyzer.pl <dump_file> <release_notes>
#

# FIXME:  at the moment this code is incomplete.  Parsing and ID of individual
# lines in the "release notes" more or less works, but assembling a functional
# database is not yet done.  The main issue is how to scoop up dangling
# extra lines of description for individual bits
#

use strict;

die "usage:  $0 <dump_file> <release_notes>" if( $#ARGV != 1);

my $dumpf = $ARGV[0];
my $relno = $ARGV[1];

open DF, "< $dumpf" or die "Opening $dumpf for reading";
open RF, "< $relno" or die "Opening $relno for reading";


my $chip;
my %chips;

# check for various forms of address specifier in a line
# return two-element list with address (or "" if none)
# and description
sub addr_header {
    my $line = shift @_;
    my @ret;
    my $addr = "";
    my $descr;
    if( $line =~ /^0x\w+\s/) {
	($addr,$descr) = $line =~ /(0x\w+)\s+(.*)$/;
    } elsif( $line =~ /^offset\s+0x/) {
	($addr,$descr) = $line =~ /^offset\s+(0x\w+)\s+(.*)$/;
    }	    
    push @ret, $addr;
    push @ret, $descr;
    return @ret;
}


# check for various forms of bit specifier in a line
# return two-element list with bit number (or "" if none)
# and description
sub bit_header {
    my $line = shift @_;
    my @ret;
    my $bit = "";
    my $descr = "";
#    print "Examining line: \"$line\"\n";
    if( $line =~ /^\s+bit/) {
	($bit,$descr) = $line =~ /^\s+bit\s+([0-9-]+)\s+(.*)$/;
#	print "  type 1 ($bit,$descr)\n";
    } elsif( $line =~ /^\s+read:\s+\d+\s/) {
	($bit,$descr) = $line =~ /^\s+read:\s+([0-9-]+)\s+(.*)$/;
#	print "  type 2 ($bit,$descr)\n";
      } elsif( $line =~ /^\s+write:\s+bit\s+([0-9-]+)/) {
	  my ($rest) = $line =~ /^\s+write:\s+bit\s+(.*)$/;
	  ($bit,$descr) = $rest =~ /([0-9-]+)\s+(.*)/;
#	  print "  type 3 ($bit,$descr)\n";
    } else {
#	print "  no match\n";
    }
    push @ret, $bit;
    push @ret, $descr;
    return @ret;
}

my $addr;
my $adescr;
my $bit;
my $bdescr;

# load up the release notes
while( !eof(RF)) {

    # look for the "<chip> memory map" header
    while( my $line = <RF>) {
	$line =~ s/[[:cntrl:]]$//g; # clean control chars at end (DOS or Unix)
	if( $line =~ /chip memory map/) {
	    ($chip) = $line =~ /^\s*(\w+)\s*chip/;
	    print "Active chip set to $chip\n";
	    last;
	}
    }

    # look for hex address
    # or bit specifiers
    while( my $line = <RF>) {
	$line =~ s/[[:cntrl:]]$//g;

	($addr, $adescr) = addr_header( $line);
	($bit, $bdescr) = bit_header($line);

	if( $addr ne "") {
	    print "Addr: [$addr]  descr: \"$adescr\"\n";
#	    print "Store as chips { $chip } { $addr } = \" $adescr \"\n";
	    $chips{$chip}{$addr}{descr} = $adescr;
	} elsif( $bit ne "") {
	    print "  bit: [$bit]  descr: \"$bdescr\"\n";
	    $chips{$chip}{$addr}{bits}{$bit} = $bdescr;
	} else {
	    print "  other: $line\n";
	    # it's dangling bit description
	    $chips{$chip}{$addr}{bits}{$bit} .= "\n" . $line;
	}

	# look for end of section
	last if( $line =~ /memory read window/ || $line =~ /initial test of AMC13/);
    }

}

print "------ Dump data structure -----\n";

my @chip_list = keys %chips;
foreach my $ch ( @chip_list ) {
    print "CHIP: $ch\n";
    my @addr_list = keys %{$chips{$ch}};
    foreach my $adr ( @addr_list) {
	my $ad = $chips{$ch}{$adr}{descr};
	print "  Addr: $adr  $ad\n";
	my @bit_list = keys %{$chips{$ch}{$adr}{bits}};
	foreach my $bt ( @bit_list ) {
	    my $bd = $chips{$ch}{$adr}{bits}{$bt};
	    print "    Bits: $bt  descr = \"$bd\"\n";
	}
    }
}
